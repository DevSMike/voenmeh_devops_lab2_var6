# voenmeh_devops_lab2_var6

## 1. Build the application with *build.sh*
```shell
chmod +x ./build.sh
./build.sh
```

## 2. Install the application with deb Linux package
```shell
cd build/
sudo apt install ./reverse-1.0.0-Linux.deb
```