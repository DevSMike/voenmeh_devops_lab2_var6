#ifndef REVERSE_H
#define REVERSE_H
#include <string>

std::string reverse_words(std::string s);

#endif //REVERSE_H
