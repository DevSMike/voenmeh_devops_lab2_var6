#include <gtest/gtest.h>
#include "reverse.cpp"

TEST(EmptyStringTest, HandlesZeroInput) {
EXPECT_EQ(reverse_words(" "),  " ");
}

TEST(ReverseWordsTest, HandlesPositiveInput) {
EXPECT_EQ(reverse_words("hello world"), "world hello");
EXPECT_EQ(reverse_words("hello world!"), "world! hello");
EXPECT_EQ(reverse_words("hello"), "hello");
EXPECT_EQ(reverse_words("my name is Mike"), "Mike is name my");

}
