#include <string>
#include <vector>
#include <algorithm>

using namespace std;

string reverse_words(string s)
{
    vector<string> words;
    size_t pos = 0;

    while ((pos = s.find(' ')) != string::npos) {
        string token = s.substr(0, pos);
        words.push_back(token);
        s.erase(0, pos + 1);
    }
    words.push_back(s);

    reverse(words.begin(), words.end());

    string result;
    for (const auto & word : words) {
        result += word + " ";
    }

    result.pop_back();
    return result;
}
